package khkautologin;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.awt.AWTException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import org.apache.commons.logging.LogFactory;

public class KHKAutoLogin {

    public static void main(String[] args) throws FailingHttpStatusCodeException, MalformedURLException, IOException, AWTException, InterruptedException {

        //Warnings weghalen
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        printLijn();
        print("Cisco autologin gemaakt door Sebastiaan Jansen");
        //http://androidyou.blogspot.be/2010/08/cisco-wireless-web-authentication-web.html
        printLijn();

        if (args.length >= 2) {
            print("Argumenten inlezen...");
            // 


            String wachtwoordBeveiligd = "";
            for (int i = 0; i < args[1].length(); i++) {
                wachtwoordBeveiligd += "*";
            }

            int minutenWachten = 15;
            if (args.length >= 3) {
                try {
                    minutenWachten = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    print("Derde argument is geen integer. Gaat verder met standaardwachttijd (15min)");
                }
            }

            // Inloggen
            print("Argumenten gelezen, inloggen als \"" + args[0] + "\" Met wachtwoord " + wachtwoordBeveiligd);
            while (true) {
                LoginWifi(args[0], args[1]);
                printLijn();
                print("Tijdje wachten tot volgende inlogpoging. (Programma afsluiten met rood kruisje)");
                print("ZZZzzzZZZzzZZZzzZZZzzZZZ");
                Thread.sleep(minutenWachten * 1000 * 60);
            }
        } else {
            // Niet inloggen
            print("Gebruikersnaam en wachtwoord als argument opgeven bij het opstarten van het programma.");
            print("Optioneel kan u de tijd in minuten als derde argument opgeven.");
        }
    }

    private static void LoginWifi(String argUsername, String argPassword) throws IOException, MalformedURLException {

        WebClient client = new WebClient();
        print("Pagina laden...");
        try {
            HtmlPage page = client.getPage("http://toledo.khk.be");
            String titelCheck = "KHKempen Toledo";
            if (page.getTitleText().contains(titelCheck)) {
                print("Reeds ingelogd, titel bevat \"" + titelCheck + "\"");
            } else {
                print("Geladen, proberen in te loggen...");
                //Formulier invullen
                HtmlElement username = page.getElementByName("username");
                HtmlElement password = page.getElementByName("password");
                username.setAttribute("Value", argUsername);
                password.setAttribute("Value", argPassword);

                HtmlElement button = page.getElementByName("Submit");
                HtmlPage p2 = button.click();
                if (p2.getTitleText().equals("Web Authentication")) {
                    print("Kon niet inloggen (waarschijnlijk verkeerde login/wachtwoord)");
                } else {
                    print("Ingelogd.");
                }
                client.closeAllWindows();
            }
        } catch (UnknownHostException e) {
            print("error: Kon host niet openen : " + e.getMessage());
            print("Dit is waarschijnlijk omdat je geen internetverbinding hebt.");
        }

    }

    private static void print(String lijn) {
        System.out.println(lijn);
    }

    private static void printLijn() {
        print("--------------------------------------------------------------------------");
    }
}
